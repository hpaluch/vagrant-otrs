# Main standalone Puppet configuration file
# Tested on CentOS 6.4 32bit

### Configurable Variables ###
$mysql_root_password = 'change-me-or-you'
$otrs_db = 'otrs'
$otrs_db_user = 'otrs'
$otrs_db_password = 'change-me-if-you-can'

## Less Configurable Variables
$otrs_package_url = 'http://ftp.otrs.org/pub/otrs/RPMS/rhel/6/otrs-3.2.10-02.noarch.rpm'

$otrs_base_packages = [
         'perl-core','perl-Crypt-SSLeay',
         'procmail','mailx',
         'httpd','mod_perl', # we don't use apache puppet module - it completely messes rpm configs in /etc/httpd/conf.d 
         'perl-DBI','perl-TimeDate','perl-Net-DNS',
	 'perl-IO-Socket-SSL',
         'perl-Convert-ASN1','perl-XML-Parser',
         'perl-GDGraph','perl-GDTextUtil', # Warning! There are two GD packages: EL6 GD (without space) and RPMFORGE GD (with spaces)
         'perl-LDAP','perl-SOAP-Lite',
         'perl-Crypt-PasswdMD5','perl-Digest-SHA',
         'perl-Devel-Symdump', # for mod_perl /perl-status

]

$otrs_epel_packages = [
         'perl-GraphViz','perl-Text-CSV_XS','perl-JSON-XS',
         'perl-Mail-IMAPClient','perl-YAML-LibYAML', # OTRS 3.2
         'perl-PDF-API2'
]

### Main section ###

# Favorite packages
package {
   [ mc,man, 'vim-enhanced' ]:
        ensure => present,
}

# MySQL Server Configuration
class { 'mysql::server':
    config_hash => {
         root_password      => $mysql_root_password,
         bind_address       => ['0.0.0.0'],
         max_allowed_packet => '32M', # required by OTRS/ITSM
         query_cache_size   => '10M', # recommended for OTRS
    }
}

# create database for OTRS
mysql::db { $otrs_db:
      user     => $otrs_db_user,
      password => $otrs_db_password,
      host     => 'localhost',
      charset  => 'utf8', # is default, but just to ensure
      grant    => ['all'],
      before   => Exec['setup otrs tables'],
}


# OTRS Client configuration
#   PERL bindings
class { 'mysql::bindings::perl':
}


# Base EL6 packages required by OTRS
package {
       $otrs_base_packages :
          ensure => present,
          before => Package[otrs],
}
 
# adds EPEL repository (see: https://github.com/example42/puppet-yum)
class { 'yum':
}

# EPEL packages required by OTRS 3.2
package {
     $otrs_epel_packages :
	ensure  => present,
	require => Class['yum::repo::epel'],
	before  => Package[otrs],
}

# OTRS package itself
package {
   "otrs":
	provider => rpm,
	source   => $otrs_package_url,
}

$mysql_base_command = '/usr/bin/mysql --defaults-extra-file=/root/.my.cnf' 
# Seeds OTRS database
exec {
      "setup otrs tables":
           command => "$mysql_base_command $otrs_db < /vagrant/stuff/puppet/files/setup_otrs_tables.sql",
           unless => "$mysql_base_command -e \"select 1 from ticket limit 1\" $otrs_db",
           require => [
               Package[otrs],
           ],
}

# Setup database connection in /opt/otrs/Kernel/Config.pm
# Warning! This is a bit hacky
exec {
     "set db in Kernel/Config.pm":
       command => "/vagrant/stuff/puppet/files/set-config-pm-value.pl Database=$otrs_db DatabaseUser=$otrs_db_user DatabasePw=$otrs_db_password SecureMode=1",
       user => otrs,
       group => apache,
       unless => "/opt/otrs/bin/otrs.CheckDB.pl",
       require => [ Exec["setup otrs tables"],
                  ]
}

Exec["set db in Kernel/Config.pm"] ~> Service[httpd]

service {
     "httpd":
        ensure => running,
        enable => true,
        hasstatus => true,
        hasrestart => true,
        require => [ Package[httpd],Package[mod_perl] ],
}
# FIX wrong ownership of some OTRS directories
file {
      [ "/opt/otrs/Kernel","/opt/otrs/Kernel/Language"]:
	ensure => directory,
        owner => otrs, 
        group => apache,
        mode => 0775, 
        require => [ Package[otrs], Package[httpd] ], # provide users/groups
}


# IP Tables macro
define my_iptables_enable_port {
 augeas { "ipt_port_$title":
    # based on https://groups.google.com/forum/#!msg/puppet-users/lP1SW0510iM/4UwHOJiBcecJ
    context => "/files/etc/sysconfig/iptables/table",
    changes => ['ins append before append[.="INPUT"][last()]',
                'defnode INPUT append[.=""] INPUT',
                'set $INPUT INPUT',
                'set $INPUT/match[1] state',
                'set $INPUT/state NEW',
                'set $INPUT/match[2] tcp',
                'set $INPUT/protocol tcp',
                "set \$INPUT/dport $title",
                'set $INPUT/jump ACCEPT'],
    onlyif => "match append[*]/dport[.=\"$title\"] size == 0",
    require => Package[iptables],
  }
  Augeas["ipt_port_$title"] ~> Service[iptables]
}
# IP Tables package
package {
       'iptables':
          ensure => present,
}

# IP Tables service
service {
     "iptables":
        ensure => running,
        enable => true,
        hasstatus => true,
	hasrestart => true,
	require => [ Package[iptables], ],
}

# enable Apache Access
my_iptables_enable_port {
      80:
}


