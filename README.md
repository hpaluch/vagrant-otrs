###OTRS VM created with Vagrant

This setups bare-bone OTRS 3.2 on CentOS 6.4/32bit

##Setup
	
	:::bash
	git clone git@bitbucket.org:hpaluch/vagrant-otrs.git
	cd vagrant-otrs
	git submodule init
	git submodule update

##Run

	:::bash
	vagrant up
	vagrant ssh   # actually not needed

On your Host point browser to http://localhost:8888/otrs/index.pl login: root@localhost password: root

That''s all!

To install ITSM use just:

	:::bash
	vagrant ssh
	# Note: The colon before ITSM-3.2.9.opm is OTRS speciality...
	su - otrs -s /bin/bash -l -c "bin/otrs.PackageManager.pl -a install -p http://ftp.otrs.org/pub/otrs/itsm/bundle32/:ITSM-3.2.9.opm"


##Credits

* Main: Henryk Paluch
* Portions copied from https://github.com/zeleznypa/vagrant.git made by Pavel Zelezny

